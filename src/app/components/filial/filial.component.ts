import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { FilialService } from './filial.service';
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-filial',
  providers: [FilialService],
  templateUrl: './filial.component.html',
  styleUrls: ['./filial.component.css']
})
export class FilialComponent implements OnInit {

  readonly filials$ = this.filialService.getFilials();
  readonly employees$ = this.filialService.getEmployees();
  readonly data$ = combineLatest([this.filials$, this.employees$]).pipe(map(([filials, employees]) => { 
    return filials.map(filial => {
      return ({
        ...filial,
        isHidden: true,
        employees: employees.filter(employer => employer.filial_id === filial.id).sort((a,b) => { if (a.name > b.name) {return 1} else {return -1}})
      })
    })
   })
   );

  constructor(private filialService: FilialService){}

  ngOnInit(): void {
    
  }

}
