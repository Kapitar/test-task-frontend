import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FilialService {
  constructor(private http: HttpClient) { }

  getFilials(): Observable<any[]> {
    return this.http.get("http://127.0.0.1:8000/api/fillials") as Observable<any[]>;
  }

  getEmployees(): Observable<any[]> {
    return this.http.get("http://127.0.0.1:8000/api/employees") as Observable<any[]>;   
  }
}